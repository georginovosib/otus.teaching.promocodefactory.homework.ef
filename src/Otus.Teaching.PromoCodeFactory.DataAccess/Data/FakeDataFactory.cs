﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static IEnumerable<Employee> Employees => new List<Employee>()
        {
            new Employee()
            {
                Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                RoleId = Roles.FirstOrDefault(x => x.Name == "Admin").Id,
                AppliedPromocodesCount = 5
            },
            new Employee()
            {
                Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                LastName = "Андреев",
                RoleId = Roles.FirstOrDefault(x => x.Name == "PartnerManager").Id,
                AppliedPromocodesCount = 10
            },
            new Employee()
            {
                Id = Guid.Parse("a766e2bf-340a-46ea-bff3-f1700b435895"),
                Email = "nikolai@somemail.ru",
                FirstName = "Николай",
                LastName = "Иванович",
                RoleId = Roles.FirstOrDefault(x => x.Name == "PartnerManager").Id,
                AppliedPromocodesCount = 15
            }
        };

        public static IEnumerable<Role> Roles => new List<Role>()
        {
            new Role()
            {
                Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                Name = "Admin",
                Description = "Администратор",
            },
            new Role()
            {
                Id = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                Name = "PartnerManager",
                Description = "Партнерский менеджер"
            }
        };

        public static IEnumerable<Preference> Preferences => new List<Preference>()
        {
            new Preference()
            {
                Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                Name = "Театр"
            },
            new Preference()
            {
                Id = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                Name = "Семья"
            },
            new Preference()
            {
                Id = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                Name = "Дети"
            },
            new Preference()
            {
                Id = Guid.Parse("99324c47-68d2-472d-abb8-33cfa8cc0c84"),
                Name = "Кинотеатр"
            }
        };

        public static IEnumerable<Customer> Customers
        {
            get
            {
                var customers = new List<Customer>()
                {
                    new Customer()
                    {
                        Id = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                        Email = "ivan_sergeev@mail.ru",
                        FirstName = "Иван",
                        LastName = "Петров",
                        //TODO: Добавить предзаполненный список предпочтений
                        //Добавлено в DataContext 
                    },
                    new Customer()
                    {
                        Id = Guid.Parse("a7c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                        Email = "petr@mail.ru",
                        FirstName = "Петр",
                        LastName = "Николаев",
                        //TODO: Добавить предзаполненный список предпочтений
                        //Добавлено в DataContext
                    },
                    new Customer()
                    {
                        Id = Guid.Parse("a8c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                        Email = "leonid@mail.ru",
                        FirstName = "Леонид",
                        LastName = "Балашов",
                        //TODO: Добавить предзаполненный список предпочтений
                        //Добавлено в DataContext
                    }
                };
                return customers;
            }
        }

        public static IEnumerable<PromoCode> PromoCodes => new List<PromoCode>()
        {
            new PromoCode()
            {
                Id = Guid.NewGuid(),
                Code = "0000011",
                BeginDate = DateTime.Now,
                EndDate = DateTime.Now.AddMonths(+1),
                PartnerName = "Sputnik",
                ServiceInfo = "ServiceInfo",
                PreferenceId = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                CustomerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0")
            },
            new PromoCode()
            {
                Id = Guid.NewGuid(),
                Code = "0000012",
                BeginDate = DateTime.Now,
                EndDate = DateTime.Now.AddMonths(+1),
                PartnerName = "Ikei",
                ServiceInfo = "ServiceInfo",
                PreferenceId = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                CustomerId = Guid.Parse("a7c8c6b1-4349-45b0-ab31-244740aaf0f0")
            },
            new PromoCode()
            {
                Id = Guid.NewGuid(),
                Code = "0000016",
                BeginDate = DateTime.Now,
                EndDate = DateTime.Now.AddMonths(+1),
                PartnerName = "Mars",
                ServiceInfo = "ServiceInfo",
                PreferenceId = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                CustomerId = Guid.Parse("a8c8c6b1-4349-45b0-ab31-244740aaf0f0")
            }
        };
    }
}