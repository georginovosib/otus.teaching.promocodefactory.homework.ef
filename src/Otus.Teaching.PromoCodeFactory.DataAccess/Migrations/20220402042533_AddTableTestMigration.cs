﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Migrations
{
    public partial class AddTableTestMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "PromoCodes",
                keyColumn: "Id",
                keyValue: new Guid("475068e7-93d4-4599-b413-dc3d99617972"));

            migrationBuilder.DeleteData(
                table: "PromoCodes",
                keyColumn: "Id",
                keyValue: new Guid("75874366-ba0e-4879-9f32-d946f9d27db5"));

            migrationBuilder.DeleteData(
                table: "PromoCodes",
                keyColumn: "Id",
                keyValue: new Guid("bebd8e7b-df30-49de-a3dd-c9f0885cc693"));

            migrationBuilder.CreateTable(
                name: "TestMigrations",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    Name = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TestMigrations", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "PromoCodes",
                columns: new[] { "Id", "BeginDate", "Code", "CustomerId", "EmployeeId", "EndDate", "PartnerName", "PreferenceId", "ServiceInfo" },
                values: new object[] { new Guid("b4bb5a6e-43b4-41ac-af31-a824ff5aa5f8"), new DateTime(2022, 4, 2, 11, 25, 32, 446, DateTimeKind.Local).AddTicks(8136), "0000011", new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"), new DateTime(2022, 5, 2, 11, 25, 32, 448, DateTimeKind.Local).AddTicks(9875), "Sputnik", new Guid("ef7f299f-92d7-459f-896e-078ed53ef99c"), "ServiceInfo" });

            migrationBuilder.InsertData(
                table: "PromoCodes",
                columns: new[] { "Id", "BeginDate", "Code", "CustomerId", "EmployeeId", "EndDate", "PartnerName", "PreferenceId", "ServiceInfo" },
                values: new object[] { new Guid("ccc23109-d881-41d0-bf93-0d346e8d4ee1"), new DateTime(2022, 4, 2, 11, 25, 32, 450, DateTimeKind.Local).AddTicks(7013), "0000012", new Guid("a7c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("f766e2bf-340a-46ea-bff3-f1700b435895"), new DateTime(2022, 5, 2, 11, 25, 32, 450, DateTimeKind.Local).AddTicks(7069), "Ikei", new Guid("c4bda62e-fc74-4256-a956-4760b3858cbd"), "ServiceInfo" });

            migrationBuilder.InsertData(
                table: "PromoCodes",
                columns: new[] { "Id", "BeginDate", "Code", "CustomerId", "EmployeeId", "EndDate", "PartnerName", "PreferenceId", "ServiceInfo" },
                values: new object[] { new Guid("f3ce3c2e-847f-4e5b-aef8-1b17da098839"), new DateTime(2022, 4, 2, 11, 25, 32, 450, DateTimeKind.Local).AddTicks(7245), "0000013", new Guid("a8c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("a766e2bf-340a-46ea-bff3-f1700b435895"), new DateTime(2022, 5, 2, 11, 25, 32, 450, DateTimeKind.Local).AddTicks(7250), "Mars", new Guid("76324c47-68d2-472d-abb8-33cfa8cc0c84"), "ServiceInfo" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TestMigrations");

            migrationBuilder.DeleteData(
                table: "PromoCodes",
                keyColumn: "Id",
                keyValue: new Guid("b4bb5a6e-43b4-41ac-af31-a824ff5aa5f8"));

            migrationBuilder.DeleteData(
                table: "PromoCodes",
                keyColumn: "Id",
                keyValue: new Guid("ccc23109-d881-41d0-bf93-0d346e8d4ee1"));

            migrationBuilder.DeleteData(
                table: "PromoCodes",
                keyColumn: "Id",
                keyValue: new Guid("f3ce3c2e-847f-4e5b-aef8-1b17da098839"));

            migrationBuilder.InsertData(
                table: "PromoCodes",
                columns: new[] { "Id", "BeginDate", "Code", "CustomerId", "EmployeeId", "EndDate", "PartnerName", "PreferenceId", "ServiceInfo" },
                values: new object[] { new Guid("475068e7-93d4-4599-b413-dc3d99617972"), new DateTime(2022, 4, 2, 11, 21, 50, 755, DateTimeKind.Local).AddTicks(2632), "0000011", new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"), new DateTime(2022, 5, 2, 11, 21, 50, 757, DateTimeKind.Local).AddTicks(2555), "Sputnik", new Guid("ef7f299f-92d7-459f-896e-078ed53ef99c"), "ServiceInfo" });

            migrationBuilder.InsertData(
                table: "PromoCodes",
                columns: new[] { "Id", "BeginDate", "Code", "CustomerId", "EmployeeId", "EndDate", "PartnerName", "PreferenceId", "ServiceInfo" },
                values: new object[] { new Guid("bebd8e7b-df30-49de-a3dd-c9f0885cc693"), new DateTime(2022, 4, 2, 11, 21, 50, 757, DateTimeKind.Local).AddTicks(7807), "0000012", new Guid("a7c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("f766e2bf-340a-46ea-bff3-f1700b435895"), new DateTime(2022, 5, 2, 11, 21, 50, 757, DateTimeKind.Local).AddTicks(7847), "Ikei", new Guid("c4bda62e-fc74-4256-a956-4760b3858cbd"), "ServiceInfo" });

            migrationBuilder.InsertData(
                table: "PromoCodes",
                columns: new[] { "Id", "BeginDate", "Code", "CustomerId", "EmployeeId", "EndDate", "PartnerName", "PreferenceId", "ServiceInfo" },
                values: new object[] { new Guid("75874366-ba0e-4879-9f32-d946f9d27db5"), new DateTime(2022, 4, 2, 11, 21, 50, 757, DateTimeKind.Local).AddTicks(8709), "0000013", new Guid("a8c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("a766e2bf-340a-46ea-bff3-f1700b435895"), new DateTime(2022, 5, 2, 11, 21, 50, 757, DateTimeKind.Local).AddTicks(8727), "Mars", new Guid("76324c47-68d2-472d-abb8-33cfa8cc0c84"), "ServiceInfo" });
        }
    }
}
