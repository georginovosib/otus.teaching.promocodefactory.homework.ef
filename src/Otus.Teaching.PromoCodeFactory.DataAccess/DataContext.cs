﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Configurations;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DataContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }
        public DbSet<TestMigration> TestMigrations { get; set; }
        public DataContext()
        {
            Database.EnsureDeleted();
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Filename = code.sqlite");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Влючаем AutoInclude()
            modelBuilder.Entity<Employee>().Navigation(e => e.Role).AutoInclude();
            modelBuilder.Entity<Role>().Navigation(r => r.Employees).AutoInclude();
            modelBuilder.Entity<Customer>().Navigation(c => c.Preferences).AutoInclude();
            modelBuilder.Entity<Customer>().Navigation(c => c.PromoCodes).AutoInclude();

            // Отношения между моделями настраиваем в отдельных классах
            modelBuilder.ApplyConfiguration(new RoleConfiguration());

            // Инициализация базы данных
            modelBuilder.Entity<Role>().HasData(FakeDataFactory.Roles);
            modelBuilder.Entity<Employee>().HasData(FakeDataFactory.Employees);
            modelBuilder.Entity<Preference>().HasData(FakeDataFactory.Preferences);
            modelBuilder.Entity<Customer>().HasData(FakeDataFactory.Customers);
            modelBuilder.Entity<PromoCode>().HasData(FakeDataFactory.PromoCodes);

            // Отношения между клиентом и предподчениями
            // Первый клиент имеет предподчения: Театр, Семья, Дети
            // Второй клиент имеет предподчения: Театр, Семья
            // Третий клиент имеет предподчения: Театр
            modelBuilder.Entity<Customer>().HasMany(c => c.Preferences).WithMany(p => p.Customers).UsingEntity(j => j.HasData(
                new { CustomersId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), PreferencesId = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c") }, 
                new { CustomersId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), PreferencesId = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd") },
                new { CustomersId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), PreferencesId = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84") },
                new { CustomersId = Guid.Parse("a7c8c6b1-4349-45b0-ab31-244740aaf0f0"), PreferencesId = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c") },
                new { CustomersId = Guid.Parse("a7c8c6b1-4349-45b0-ab31-244740aaf0f0"), PreferencesId = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd") },
                new { CustomersId = Guid.Parse("a8c8c6b1-4349-45b0-ab31-244740aaf0f0"), PreferencesId = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c") }));
        }
    }
}
 