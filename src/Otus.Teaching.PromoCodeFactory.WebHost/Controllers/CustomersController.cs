﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customersRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersController(IRepository<Customer> customersRepository, IRepository<Preference> preferenceRepository)
        {
            _customersRepository = customersRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить данные всех клиентов 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<CustomerShortResponse>> GetCustomersAsync()
        {
            //TODO: Добавить получение списка клиентов
            var customers = await _customersRepository.GetAllAsync();

            var customerModelList = customers.Select(x => new CustomerShortResponse(x)).ToList();

            return customerModelList;
        }

        /// <summary>
        /// Получить данные клиента по Id вместе с предпочтениями и выданными ему промомкодами
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerByIdWithPreferencesAsync(Guid id)
        {
            //Подключить связь с предпочтениями
            //await _customersRepository.GetByIdAsync(id, "Preferences");
            //TODO: Добавить получение клиента вместе с выданными ему промомкодами
            var customer = await _customersRepository.GetByIdAsync(id);
            
            if (customer == null)
                return NotFound();

            var customerModel = new CustomerResponse(customer);

            return customerModel;
        }

        /// <summary>
        /// Добавить нового клиента вместе с его предпочтениями
        /// </summary>
        /// <param name="FirstName"></param>
        /// <param name="LastName"></param>
        /// <param name="Email"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(string FirstName, string LastName, string Email)
        {
            //TODO: Добавить создание нового клиента вместе с его предпочтениями

            Customer customer = new Customer() { Id = System.Guid.NewGuid(), 
                FirstName = FirstName, LastName = LastName, Email = Email};

            await _customersRepository.CreateAsync(customer);

            //Подключить связь с предпочтениями
            var customerNew = await _customersRepository.GetByIdAsync(customer.Id);

            //Получаем предподчение "Театр"
            var preferences = await _preferenceRepository.GetAllAsync();
            var preferenceKino = preferences.FirstOrDefault(x => x.Name == "Театр");

            //Добавляем предподчтение "Театр" к выбранному по Id клиенту
            customerNew.Preferences.Add(preferenceKino);


            return Ok("Добавлен клиент");
        }

        /// <summary>
        /// Обновить данные клиента вместе с его предпочтениями и промокодами
        /// </summary>
        /// <param name="id"></param>
        /// <param name="FirstName"></param>
        /// <param name="LastName"></param>
        /// <param name="Email"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateCustomersAsync(Guid id , string FirstName, string LastName, string Email)
        {
            var customer = await _customersRepository.GetByIdAsync(id);
            //CreateOrEditCustomerRequest request
            if (customer == null) return NotFound("Клиента с таким id не существует");

            var preferences = await _preferenceRepository.GetAllAsync();
            var preferenceKino = preferences.FirstOrDefault(x => x.Name == "Кинотеатр");

            if (FirstName != null) customer.FirstName = FirstName;
            if (LastName != null) customer.LastName = LastName;
            if (Email != null) customer.Email = Email;
            //Добавляем предподчтение "Кинотеатр" к выбранному по Id клиенту
            customer.Preferences.Add(preferenceKino);

            await _customersRepository.UpdateAsync(customer);

            //TODO: Обновить данные клиента вместе с его предпочтениями
            return Ok("Данные обновлены");
        }

        /// <summary>
        /// Удалить клиента вместе с выданными ему промокодами
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customersRepository.GetByIdAsync(id);

            await _customersRepository.DeleteAsync(customer);

            //TODO: Удаление клиента вместе с выданными ему промокодами
            return Ok("Клиент удален");
        }
    }
}