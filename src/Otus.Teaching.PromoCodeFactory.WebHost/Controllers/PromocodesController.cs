﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promocodeRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<Customer> _customersRepository;

        public PromocodesController(IRepository<PromoCode> promocodeRepository,
            IRepository<Preference> preferenceRepository,
            IRepository<Customer> customersRepository)
        {
            _promocodeRepository = promocodeRepository;
            _preferenceRepository = preferenceRepository;
            _customersRepository = customersRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promocodeAll = await _promocodeRepository.GetAllAsync();

            var promocodeShort = promocodeAll.Select(x => new PromoCodeShortResponse(x)).ToList();

            return promocodeShort;
            //TODO: Получить все промокоды 
            //throw new NotImplementedException();
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с запрашиваемым предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            // Взять из запроса запрашиваемое предподчтение
            string preference = request.Preference;
            // Найти все предподчения

            var preferences = await _preferenceRepository.GetAllAsync();
            
            // Найти запрашиваемое предподчтение
            var preferenceAsk = preferences.FirstOrDefault(p => p.Name == preference);

            // Найти всех клиентов с их предпочтениями
            var customersAll = await _customersRepository.GetAllAsync();

            // Найти всех клиентов с запрашиваемым предпочтением
            List<Customer> customerAskPreference = new List<Customer>();
            foreach (var customer in customersAll)
            {
                foreach (var p in customer.Preferences.Where(p => p.Name == preference))
                {
                    customerAskPreference.Add(customer);
                }
            }

            // Выдать промокод клиентам с запрашиваемым предпочтением
            foreach (var customer in customerAskPreference)
            {
                await _promocodeRepository.CreateAsync(new PromoCode()
                {
                    Id = Guid.NewGuid(),
                    Code = "15",
                    BeginDate = DateTime.Now,
                    EndDate = DateTime.Now.AddMonths(+1),
                    PartnerName = "Alice",
                    ServiceInfo = "serviceInfo",
                    CustomerId = customer.Id,
                    PreferenceId = preferenceAsk.Id
                });
            }


            //TODO: Создать промокод и выдать его клиентам с указанным предпочтением
            //throw new NotImplementedException();
            return Ok("Промокод выдан");
        }
    }
}