﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.DataAccess;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Роли сотрудников
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class RolesController : ControllerBase
    {


        private readonly IRepository<Role> _rolesRepository;

        public RolesController(IRepository<Role> rolesRepository)
        {
            _rolesRepository = rolesRepository;
        }

        /// <summary>
        /// Получить все доступные роли сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<RoleItemResponse>> GetRolesAsync()
        {
            var roles = await _rolesRepository.GetAllAsync();

            var rolesModelList = roles.Select(x =>
                new RoleItemResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList();

            return rolesModelList;
        }

        /// <summary>
        /// Получить роль по Id и имена всех сотрудников с этой ролью
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<RoleWithListEmployees>> GetRoleByIdWithEmployeesAsync(Guid id)
        {
            var role = await _rolesRepository.GetByIdAsync(id);
            
            if (role == null) return NotFound();

            var roleModel = new RoleWithListEmployees(role);

            return roleModel;
        }

        /// <summary>
        /// Добавить роль
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateRoleAsync(string name, string description)
        {
            Role role = new Role() { Id = System.Guid.NewGuid(), Name = name, Description = description };

            await _rolesRepository.CreateAsync(role);

            return Ok("Добавлена роль");
        }

        /// <summary>
        /// Удалить роль
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> DeleteRoleAsync(Guid id)
        {
            var role = await _rolesRepository.GetByIdAsync(id);

            await _rolesRepository.DeleteAsync(role);

            return Ok("Роль удалена");
        }
        /// <summary>
        /// Обновить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpPost("Update/{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> UpdateRoleAsync(Guid id, string name, string description)
        {
            var role = await _rolesRepository.GetByIdAsync(id);

            if (role == null) return NotFound("Роли с таким id не существует");

            if (name != null) role.Name = name;

            if (description != null) role.Description = description;

            await _rolesRepository.UpdateAsync(role);

            return Ok("Данные обновлены");
        }
    }
}