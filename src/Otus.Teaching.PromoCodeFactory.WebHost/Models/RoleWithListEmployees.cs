﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class RoleWithListEmployees
    {
        public RoleWithListEmployees(Role role)
        {
            Id = role.Id;
            Name = role.Name;
            Description = role.Description;
            Employees = role.Employees.Select(x => x.FullName).ToList();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public List<string> Employees { get; set; }
    }
}
