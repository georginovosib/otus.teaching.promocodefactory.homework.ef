﻿using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeResponse
    {
        public EmployeeResponse(Employee employee)
        {
            Id = employee.Id;
            Email = employee.Email;
            Role = new RoleItemResponse()
            {
                Id = employee.Id,
                Name = employee.Role.Name,
                Description = employee.Role.Description
            };
            FullName = employee.FullName;
            AppliedPromocodesCount = employee.AppliedPromocodesCount;
        }

        public Guid Id { get; set; }
        public string FullName { get; set; }

        public string Email { get; set; }

        public RoleItemResponse Role { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
}