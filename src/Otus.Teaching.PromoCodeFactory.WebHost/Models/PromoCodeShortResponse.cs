﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PromoCodeShortResponse
    {
        public PromoCodeShortResponse(PromoCode promoCode)
        {
            Id = promoCode.Id;
            PartnerName = promoCode.PartnerName;
            Code = promoCode.Code;
            ServiceInfo = promoCode.ServiceInfo;
            BeginDate = DateTime.Now;
            EndDate = DateTime.Now.AddMonths(+1);
            CustomerId = promoCode.CustomerId;
        }

        public Guid Id { get; set; }

        public string Code { get; set; }

        public string ServiceInfo { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        public string PartnerName { get; set; }

        public Guid? CustomerId { get; set; }
    }
}