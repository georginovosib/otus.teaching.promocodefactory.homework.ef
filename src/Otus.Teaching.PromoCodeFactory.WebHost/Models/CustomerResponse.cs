﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class CustomerResponse
    {
        public CustomerResponse(Customer customer)
        {
            Id = customer.Id;
            FirstName = customer.FirstName;
            LastName = customer.LastName;
            Email = customer.Email;
            Preference = customer.Preferences.Select(x => x.Name).ToList();
            PromoCode = customer.PromoCodes.Select(x => x.Code).ToList();
        }

        public Guid Id { get; set; }
        public string FirstName { get; set; } 
        public string LastName { get; set; }
        public string Email { get; set; }
        //TODO: Добавить список предпочтений

        public List<string> Preference { get; set; }
        public List<string> PromoCode { get; set; }
        //public List<PromoCodeShortResponse> PromoCodes { get; set; }
    }
}