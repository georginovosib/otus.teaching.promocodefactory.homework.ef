﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PreferenceShortResponse
    {
        public PreferenceShortResponse(Preference preference)
        {
            Name = preference.Name;
        }

        public string Name { get; set; }
    }
}
