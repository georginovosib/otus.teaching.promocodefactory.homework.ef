﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class TestMigration : BaseEntity
    {
        [MaxLength(30)]
        public string Name { get; set; }
    }
}
