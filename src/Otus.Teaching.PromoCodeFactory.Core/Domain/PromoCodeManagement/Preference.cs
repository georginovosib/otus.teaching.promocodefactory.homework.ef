﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference
        : BaseEntity
    {
        [MaxLength(30)]
        public string Name { get; set; }

        public virtual ICollection<Customer> Customers { get; set; }

        public virtual ICollection<PromoCode> PromoCodes { get; set; }
    }
}